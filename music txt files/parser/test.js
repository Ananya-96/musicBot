var fs = require("fs");
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/music";
var myArr = new Array();
const request = require('request');
const wordNovember = "Nov";
const wordDecember = "Dec";
const WordJanuary ="Jan";
let fileName = './mylapore fine arts.txt'

function processSchedule(fileName) {
    let fileContents = fs.readFileSync(fileName, {
        encoding: 'utf-8'
    })

    let lines = fileContents.split('\n');
    let formattedDate = ''
    let count = 0
    let currentLineNo = 0;
    do {
        let line = lines[currentLineNo];
        console.log(`line: ${line}`)
        if (line.includes(wordNovember) || line.includes(wordDecember) ||line.includes(WordJanuary)) {
            formattedDate = formatDate(line)
            console.log(`formattedDate: ${formattedDate}`)
            let nextDayLineNumber = getLineNumberOfNextDay(lines, currentLineNo)
            console.log(`nextDayLineNumber: ${nextDayLineNumber}`)
            count = nextDayLineNumber - currentLineNo - 1;
            console.log(`count: ${count}`)
            var nextLineNo = currentLineNo + 1;
            for (j = 0; j < count - 1; j++) {
                var timeline = lines[nextLineNo + j];
                var timeArr = timeline.split("-");
                var startTime = timeArr[0];
                //var endTime = timeArr[1];
                var txt = timeArr[1];
                let title = extractTitle(txt)
                let category = getCategory(title)

                let artistArr = txt.split(",");
                let otherArtist = []
                var artistName = artistArr[0];
                var firstOccurenceOfOpenBraces = artistName.indexOf('(');

                var artistWithSpace = artistName.substr(0, firstOccurenceOfOpenBraces);
                 var artist=artistWithSpace.trim();
                if (artistArr.length > 1) {
                    otherArtist = artistArr[1];
                }
                var artistUrl = 'https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch='+artist+'&format=json&srprop=snippet'
               
                var sabhaname = "Mylapore Fine Arts";
                var location = "45, Musiri Subramaniam Road, Mylapore, Chennai 600 004"
                var myobj = { sabhaname: sabhaname, date: formattedDate, startTime: startTime, category: category, artistName: artist, otherArtist: otherArtist, title: title, sabhaLocation:location };
                getWikiURL(myobj, artistUrl)
            }
        }
        currentLineNo = currentLineNo + count + 1
    } while (currentLineNo < lines.length)


}

function getWikiURL(myObj, artistUrl) {
    request(artistUrl, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        let url = ''
        if (body) {
                if(body.query)
            if (body.query.search[0]) {

                var response = body.query.search[0].title;
                if (response && response.trim().length > 0) {
                    if(response==myObj.artistName){
                        url = 'https://en.wikipedia.org/wiki/' + response
                    }
                        
                }
            }

        }
        myObj.aboutArtist = url
        console.log(myObj)
        saveToDb(myObj);
    })
}

function extractTitle(txt) {
    
    let init = txt.indexOf('(');
    let fin = txt.indexOf(')');
    let title = txt.substr(init + 1, fin - init - 1);
    return title;
}

function getCategory(title) {
    if (title != "Vocal") {
        category = "Instrumental";
    } else {
        category = "Vocal";
    }
    return category
}

function getLineNumberOfNextDay(lines, lineNumber) {
    lineNumber++;
    line = lines[lineNumber];
    while ((line.includes(wordNovember) == false && line.includes(wordDecember) == false && line.includes(WordJanuary) == false )) {
        lineNumber++;
        line = lines[lineNumber];
        if (lineNumber == lines.length) {
            break;
        }
    }
    return lineNumber
}

function formatDate(line) {
    let month;
    let mon = line.split('.')[0];
    let rest = line.split('.')[1];
    let date = rest.split(',')[0];
    var year
    if (mon == wordNovember) {
        month = "11";
         year = "2017";
    }
    if (mon == wordDecember) {
        month = "12";
         year = "2017";
    }
    if(mon=="Jan"){
        month="01"
         year = "2018";
    }
    let formattedDate = date + "-" + month + "-" + year;
    return formattedDate
}

async function saveToDb(event) {
    await MongoClient.connect(url, function (err, db) {
        if (err) throw err;

        db.collection("musicDecember").insert(event, function (err, res) {
            if (err) throw err;
            console.log("1 document inserted");
            db.close();
        });
    });
}

processSchedule(fileName)