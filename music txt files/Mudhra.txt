Dec. 8, 2017
4.00 p.m. - Sembanarkoil S.R.G.S.Mohandas & Party (Nadaswaram)
6.15 p.m. - V. V. Subramanyam & V.V.S.Murari (Violin Duet),Thiruvarur Vaidhyanathan (Mridangam), K.V.Gopalakrishnan (Kanjeera)

Dec. 9, 2017
6.15 p.m. - Trichur Brothers (Vocal), Srikrishna Mohan & Ramkumar Mohan (Vocal), Mullaivasal G.Chandramouli (Violin), Trichur R.Mohan (Mridangam), Dr.Nerkunam Sankar (Kanjira)

Dec. 10, 2017
6.15 p.m. - Ranjani gayatri (Vocal), L.Ramakrishnan (Violin), Delhi Sairam (Mridangam), Chandrasekara Sarma (Ghatam)

Dec. 11, 2017
6.15 p.m. - K. N. Renganatha Sarma (Vocal),Vaibhav Ramani (Violin), Krishna Pawan Kumar ( Mridangam), Madipakkam Murali (Ghatam)

Dec. 12, 2017
6.15 p.m. - N. Sivaganesh (Vocal), V.L.Kumar (Violin), Palladam Ravi (Mridangam), B.S.Purushotham (Kanjira)

Dec. 13, 2017
6.15 p.m. - Anahita & Apoorva (Vocal), Thirucherai Karthik (Violin), Sunaada Krishna Amai (Mridangam), Sunil Kumar (Kanjira)

Dec. 14, 2017
6.15 p.m. - Mahathi (Vocal),K.J.Deelip (Violin), N.C.Bharadwaj (Mridangam), Chandrasekara Sarma (Ghatam)

Dec. 15, 2017
6.15 p.m. - Nagai Muralidharan & Nagai Sriram (Violin Duet), Trichy Harikumar (Mridangam), Udupi Sridhar (Ghatam)

Dec. 16, 2017
6.15 p.m. - Nithyasree Mahadevan (Vocal), M.R.Gopinath (Violin), Cherthalai Ananthakrishnan (Mridangam), A.S.Krishnan (Moharsing)

Dec. 17, 2017
6.15 p.m. - Ganesh Krupa(Light Music)

Dec. 18, 2017
6.15 p.m. - Sudha Ragunathan (Vocal), Charumathi Raghuraman (Violin), Neyveli Skandasubramaniam (Mridangam), Raman (Moharsing)

Dec. 19, 2017
6.15 p.m. - Ramakrishnan Murthy (Vocal), T.K.V. Ramanujacharulu (Violin), Dr.Trichy Sankaran (Mridangam), K.V.Gopalakrishnan (Kanjira)

Dec. 20, 2017
6.15 p.m. - Adithya Madhavan (Vocal), B.V.Raghavendra Rao (Violin), V.V.Ramanamurthy (Mridangam), Thirupunithura Radhakrishnan (Ghatam)

Dec. 21, 2017
6.15 p.m. - S. Shashank (Flute), H.N.Bhaskar (Violin), Parupalli Phalgun (Mridangam), Giridhar Udupa (Ghatam)

Dec. 22, 2017
6.15 p.m. - Mysore Nagaraj & Mysore Manjunath (Violin Duet), Thiruvarur Bakthavatsalam (Mridangam), V.Suresh (Ghatam)

Dec. 23, 2017
6.15 p.m. - Neyveli Santhanagopalan (Vocal), Delhi Sunderrajan (Violin), Mudhra Bhaskar (Mridangam), Dr.S.Karthick (Ghatam)

Dec. 24, 2017
6.15 p.m. - Crazy Mohan (Drama), Return of Crazy Thieves

Dec. 25, 2017
6.15 p.m. - O. S. Arun (Bhajan), Karaikal Venkatasubramaniam (Violin), A.V.Manikandan (Mridangam), Dr.S.Karthick (Ghatam), Srikrishnan (Harmonium), Praveen (Tabla), Selvam (Thalam)

Dec. 26, 2017
6.15 p.m. - O. S. Thiagarajan (Vocal), Nagai Muralidharan (Violin), Mannargudi A. Easwaran (Mridangam), Anirudh Athreya (Kanjira)

Dec. 27, 2017
6.15 p.m. - Kadri Gopalnath (Saxophone), A.Kanyakumari (Violin), B.Harikumar (Mridangam), Rajendra Nakod (Tabla), Bangalore Rajasekar (Moharsing)

Dec. 28, 2017
6.15 p.m. - P. Unnikrishnan (Vocal),L. Ramakrishnan (Violin), B.Harikumar (Mridangam), S.V.Ramani (Ghatam)

Dec. 29, 2017
6.15 p.m. - Sheela Unnikrishnan(Dance)

Dec. 30, 2017
6.15 p.m. - Rajhesh Vaidhya (Veena), Karakuruchi Mohanraman (Mridangam), D.Chandrajit (Tabla), Vijayan (Keyboard), Sai Hari (Ghatam), Karakurichi Subramaniam (Spl Effects)

Dec. 31, 2017
6.15 p.m. - Sikkil Gurucharan (Vocal),Nagai Muralidharan (Violin), Dr.Umayalpuram K.Sivaraman (Mridangam), Sree Sundarkumar (Kanjira)

Jan. 1, 2018
9.45 a.m. -  60 women artistes(Sree Thyagaraja Pancharatnam)

Jan. 1, 2018
6.00 p.m. - Neyveli Santhanagopalan (Lec Dem)
6.00 p.m. -  Gayathri Girish (Vocal One Raga One Kriti concert), V.V.S.Murali (Violin), Sherthalai Ananthakrishnan (Mridangam)

Jan. 2, 2018
6.00 p.m. -  Prof. T.V. Gopalakrishnan (Lec Dem)
6.00 p.m. -  Sriranjani Santhanagopalan (Vocal One Raga One Kriti concert), Apoorva Krishna (Vocal), Vijay Natesan (Mridangam)

Jan. 3, 2018
6.00 p.m. - R.S. Jayalakshmi (Lec Dem)
6.00 p.m. - V. Sankaranarayanan (Vocal (One Raga One Kriti concert)), B.U.Ganesh Prasad (Violin), R.Sankaranarayanan (Mridangam)

Jan. 4, 2018
6.00 p.m. -  Suguna Varadachari (Lec Dem)
6.00 p.m.  -  Aishwarya Sankar (Vocal (One Raga One Kriti concert)), M.Vijay (Violin), Kallidaikuruchi Sivakumar (Mridangam)

Jan. 5, 2018
6.00 p.m. -  Sriram Parasuram (Lec Dem)
6.00 p.m. -  Vishnudev Namboothiri (Vocal (One Raga One Kriti concert)), L.Ramakrishnan (Violin), Trivandrum V. Balaji (Mridangam)

Jan. 6, 2018
6.00 p.m. - Ritha Rajan (Lec Dem)
6.00 p.m. - Ananya Ashok (Vocal (One Raga One Kriti concert)), Sruthi Sarathy (Violin), Praveen Sparsh (Mridangam)

Jan. 7, 2018 
5 p.m. - Kunnakudi Balamuralikrishna (Vocal),Akkarai Subbulakshmi (Violin), B.Sivaraman (Mridangam), Sree Sundar Kumar (Kanjira)
