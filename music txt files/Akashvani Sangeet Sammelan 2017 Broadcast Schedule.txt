Nov. 4, 2017 
9.30 p.m. - 10.30 p.m. - Bhimana Jadhav (Sundri)
10.30 p.m. - 11 p.m. - Anuradha Pal (Tabla)

Nov. 5, 2017
9.30 p.m. - 11 p.m. - Subra Guha (Vocal)

Nov. 6, 2017
10 p.m. - 11 p.m. - S.R. Mahadeva Sharma & S.R. Rajashree (Violin Duet)

Nov. 7, 2017
10 p.m. - 11 p.m. - Debashish Chakraborty (Guitar)

Nov. 8, 2017
10 p.m. - 11 p.m. - V.L. Tulasi Vishwanath (Vocal)

Nov. 9, 2017
10 p.m. - 11 p.m. - Pt. Dinkar Panshikar (Vocal)

Nov.10, 2017
10 p.m. - 11 p.m. - Ustad Kamaal Sabri (Sarangi)

Nov. 11, 2017
9.30 p.m. - 11 p.m. - Sudha Ragunathan (Vocal)

Nov. 12, 2017
9.30 p.m. - 10.30 p.m. - Subhendra Rao (Sitar)
10.30 - 11 p.m. - Pt.Madan Mohan Upadhyay & Prithvi Rajkumar (Tabla)

Nov. 13, 2017
10 p.m - 11 p.m. - Pt. Nirmalya Dey (Dhrupad / Dhamar)

Nov. 14, 2017
10 p.m - 11 p.m. - Abdul Sallam Naushad (Clarionet)

Nov. 15, 2017
10 p.m - 11 p.m. - Sudha Raghuraman (Vocal)

Nov. 16, 2017
10 p.m - 11 p.m. - Dr. Mrityunjay H. Agadi (Vocal)

Nov. 17, 2017
10 p.m - 11 p.m. - P. Ganesh (Chitravina)

Nov. 18, 2017
9.30 p.m. - 11 p.m. - Pt. Ulhas Kashalkar (Vocal)

Nov. 19, 2017
10 a.m. - 11 a.m. - Nishad Bakre (Vocal)
11 a.m. - 12 noon - Jaydeep Ghosh (Sarod)
9.30 p.m. - 10.30 p.m. - J. A. Jayanth (Flute)
10.30 p.m. - 11 a.m. - Patri Satish Kumar (Mridangam)

Nov. 20, 2017
10 p.m - 11 p.m. - Ashwin Srinivasan (Flute)

Nov. 21, 2017
10 p.m. - 11 p.m. - Pt. Sandipan Samajpati (Vocal)

Nov. 22, 2017
10 p.m. - 11 p.m. - Mysore S. Rajalakshmi (Veena)

Nov. 23, 2017
10 p.m. - 11 p.m. - Mukesh Sharma (Sarod)

Nov. 24, 2017
10 p.m. - 11 p.m. - Pt. Manojit Mallick (Vocal)

Nov. 25, 2017
10 p.m. - 11 p.m. - Pt. Bhajan Sopori (Santoor)

Nov. 26, 2017 
10 p.m. - 11 p.m. - Hyderabad Brothers D. Seshachary & D. Raghavachary (Vocal Duet)

Nov. 27, 2017
10 p.m. - 11 p.m. - Arun Morone (Sitar)

Nov. 28, 2017
10 p.m. - 11 p.m. - Pt. Damodar Hota (Vocal)

Nov. 29, 2017
10 p.m. - 11 p.m. - Vasudha Ravi (Vocal)

Nov. 30, 2017
10 p.m. - 11 p.m. - Pt. Soumitra Lahiri (Sitar)

Dec. 1, 2017
10 p.m. - 11 p.m. - Kudamaloor Janardanan (Flute)

Dec. 2, 2017
9.30 p.m. - 11 p.m. - Pt. Santanu Bhattacharya (Vocal)

Dec. 3, 2017
10 p.m - 11 p.m. - Rajender Prasad (Memorial Lecture)

Dec. 4, 2017
10 p.m - 11 p.m. - Nandita Chakravarty (Geet / Bhajan), Dharmesh Nargotra (Ghazal), Sharada Sinha (Bhojpuri Lokgeet)

Dec. 5, 2017
10 p.m - 11 p.m. - Shadaab Sabri & Party (Qawwali), Pravin Makwana (Gujarati Folk Music), Shanti Bai Chelak (Pandvani Gayan)

Dec. 6, 2017
10 p.m. - 11 p.m. - Sita Ram Singh (Ghazal), Kusha & Chinmei Sharma (Dogri Folk Loksangeet)

Dec. 7, 2017
10 p.m. - 11 p.m. - P. Sudha Rani (Vocal), Light Music (Telugu)

Dec. 8, 2017
10 p.m. - 11 p.m. - Vani satish (Devotional), M. Ashok (Light Music Tamil), Kalanilayam Rajeevan, Kottakkal Madhu (Kathakali Padangal)

Dec. 9, 2017
9.30 p.m. - 11 p.m. - D. Srinivas (Veena), V. Nagaraju (Venu), Suryadeepti (Violin)

Dec. 10, 2017
10 a.m. - 11 a.m. - Anuradha Kuber (Vocal)
11 a.m. - 12 noon - Subhash Vanage (Guitar)
9.30 p.m. - 10.30 p.m. - M.S. Sheela (Vocal)
10.30 p.m. - 11 p.m. - M. Muniratnam (Dolu)

Dec. 11, 2017
10 p.m. - 11 p.m.  - Ustad Mohammad Yaqoob Sheik & Suffiana Music, Ustad Abdul Rashid Hafiz & Party (Kalaa e neyame)

Dec. 12, 2017
10 p.m. - 11 p.m. - Ustad Mustafa Raza (Vichitra Veena)

Dec. 13, 2017
10 p.m.- 11 p.m. - Kuhu Ganguli Mukharjee (Vocal)

Dec. 14, 2017
10 p.m. - 11 p.m. - K. N. Renganatha Sarma (Vocal)

Dec. 15, 2017
10 p.m. - 11 p.m. - Adayar S. Jayaraman (Nadaswaram)
