Dec. 13, 2017 - Wednesday
3.30 p.m. - Mangala Isai
7.00 p.m. - ‘Thanjavur Quartette – Revisited’ - by Leela Samson & Spanda

Dec. 14, 2017 - Thursday
7.00 a.m. - O. S. Sundar(Namasankeerthanam)
4.00 p.m. - Geetha Krishnamurthy (Veena) & Party
7.00 p.m. - Swarnamalya Ganesh (Dance)
3.00 p.m. - Manasa Sriram (Bharatanatyam)
4.45 p.m. - Subashri Sasidharan (Bharatanatyam)

Dec. 15, 2017 - Friday
7.00 a.m. - Udayalur K. Kalyanarama Bagavathar(Namasankeerthanam)
1.30 p.m. - Lakshmi Parthasarathy Athreya (Bharatanatyam)
4.00 p.m. - Sikkil Gurucharan (Vocal), H.N.Bhaskar (Violin), Dr.Trichy Sankaran (Mridangam), Anirudh Athreya (Kanjira)
7.00 p.m. - Srekala Bharath (Bharatanatyam)
10.00 a.m. - Mala Ramadorai(Hindustani)
3.00 p.m. - Tejashri Kannan (Bharatanatyam)
4.45 p.m. - Maithreyi Suresh Iyer (Bharatanatyam)

Dec. 16, 2017 - Saturday
9.30 a.m. - Visaka Hari (Musical Discourse) 
1.30 p.m. - Lavanya Sankar (Bharatanatyam)
4.00 p.m. - Lalgudi Vijayalakshmi (Violin Solo), Patri Satishkumar (Mridangam), G.Guruprasanna (Ghatam)
10.00 a.m. - Patri Satish Kumar & Poovalur Srinivasan (Mrudangam Duet)
3.00 p.m. - Krishnalaxmi (Bharatanatyam)
4.45 p.m.- Anamika Suresh (Mohiniattam)

Dec. 17, 2017 - Sunday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar (Maha Bhaktha Vijayam)
9.30 a.m. - Visaka Hari (Musical Discourse)
1.30 p.m. - Renjith Babu & Vijna Vasudevan (Bharatanatyam)
4.00 p.m. - N. Vijay Siva (Vocal), Nagai Sriram (Violin), Bombay C.N.Balaji (Mridangam), V.Suresh (Ghatam)
7.00 p.m. - Bharathanjali & Cleveland Aradhana Presents ‘Vijayathe Gopala Choodamani’ Part -1
10.00 a.m. - Vittal Ramamurthy (Violin), J.Vaidyanathan (Mridangam), N.Guruprasad (Ghatam)
3.00 p.m. - R. Harini (Bharatanatyam)
4.45 p.m. - Laxminarayan Jena (Kathak)

Dec. 18, 2017 - Monday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
1.30 p.m. - Jyothsna Jagannathan (Bharatanatyam)
4.00 p.m. - Kadri Gopalnath (Saxaphone), A.Kanyakumari (Violin), B.Harikumar (Mridangam), Rajendra Nakod (Tabla),  B.Rajasekar (Morsing)
7.00 p.m.- R. Vineeth (Bharathanatyam)
10.00 a.m. - Swarna Rethas (Vocal), Raghul (Violin), Akshay Ananthapadmanabhan (Mridangam)
3.00 p.m. - Ashwini Senthil, Kavya Kameshwari & Amritha Padmanaban (Bharatanatyam)
4.45 p.m. - C. M. Devaprayaga (Mohiniattam)

Dec. 19, 2017 - Tuesday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
1.30 p.m. - Aishwarya Balasubramanian (Bharatanatyam)
4.00 p.m. - Aruna Sairam (Vocal), B.V.RaghavendraRao (Violin),Tiruvarur Vaidyanathan (Mridangam),  S.V.Ramani (Ghatam)
7.00 p.m. - P. Praveenkumar (Mridangam)
10.00 a.m. - Jayashree Vaidyanathan (Vocal), Meera Sivaramakrishnan (Violin), Manikudi Chandrasekaran (Mridangam), N.Narasimhan (Ghatam)
3.00 p.m. - Vaishali Chandrasekhar (Bharatanatyam)
4.45 p.m. - Dhamayanthi (Bharatanatyam)

Dec. 20, 2017 - Wednesday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
1.30 p.m. - Navia Natarajan (Bharatanatyam)
4.00 p.m. - N. Ravikiran (Chitravina ),Lalgudi GJR Krishnan (Violin ), Srimushnam V.RajaRao (Mridangam),  S.V.Ramani (Ghatam)
7.00 p.m. - Sreelatha Vinod (Bharatanatyam)
10.00 a.m. - Jayashri Jayaramakrishnan (Vocal), Srilakshmi Venkataramani (Violin), Hanumanthapuram J.Buvarahan (Mridangam)
3.00 p.m. - Anjali Hariharan (Bharatanatyam)
4.45 p.m. - Dhivya S. Pillai (Bharatanatyam)

Dec. 21, 2017 - Thursday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
1.30 p.m. - Parshwanath. S. Upadhye (Bharatanatyam)
4.00 p.m. - Malladi Brothers(Vocal), EmbarKannan (Violin), K.V.Prasad (Mridangam), Vazhappally R.Krishnakumar (Ghatam)
7.00 p.m. - Rama Vaidyanathan (Bharatanatyam)
10.00 a.m. - A. G. Gnanasundaram & M.Vijay Ganesh (Violin Duet), Delhi Sairam (Mridangam),  Adambakkam Shankar (Ghatam)
3.00 p.m. - G. Reshma (Kuchipudi)
4.45 p.m. - Samyuktha (Bharatanatyam)

Dec. 22, 2017 - Friday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
1.30 p.m. - Ragini Chandrasekar (Bharatanatyam)
4.00 p.m. - Hyderabad Brothers(Vocal), H.K.Venkatram (Violin), Kotipalli Ramesh (Mridangam), Papanasam S.Sethuraman (Kanjira)
7.00 p.m. - Alarmelvalli (Bharatanatyam)
10.00 a.m. - Saashwathi Prabhu (Vocal), J.B.SruthiSagar (Flute), S.J.Arjun Ganesh (Mrudangam), N.Sundar (morsing)
3.00 p.m. - Krithika .S (Bharatanatyam)
4.45 p.m. - Swathi M.Kumar (Bharatanatyam)

Dec. 23, 2017 - Saturday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
4.00 p.m. - Vignesh Ishwar (Vocal), R.K.Shriramkumar (Violin), Dr.Umayalpuram K.Sivaraman (Mridangam), B.ShreeSundar Kumar (Kanjira)
7.00 p.m. - Shobana (Bharatanatyam)
10.00 a.m. - Shankar Ramani (Vocal), H.V.Raghuram (Violin), B.Sivaraman (Mridangam)
3.15 p.m. - Savitha Sundarsan (Vocal) , Vijay Muralidharan (Violin), SaiSarangan (Mridangam)
5.00 p.m. - Yatheesan Selvakumar (Flute) , Anayampatti Venkatasubramaniam  (Violin) , Kumar Kanthan (Mridangam)
6.45 p.m. - Ashwin Srikant (Vocal), V.Sanjeev (Violin), S.Aravind (Mridangam )

Dec. 24, 2017 - Sunday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
4.00 p.m. -  R.Kumaresh (Violin) , Dr.Jayanthi Kumaresh (Veena), Anantha R.Krishnan (Mridangam), Trichy Krishnaswamy (Ghatam)
7.00 p.m. - Uma Nambudripad Sathyanarayanan (Bharatanatyam)
3.15 p.m. - Hrishikesh & Priyanka Chary (Veena Duet) , P.Kirupakaran (UK) –Mridangam , N.Guruprasad – Ghatam
5.00p.m. - Vidhya Kanthan (Vocal) , Karaikal Venkatasubramaniam – Violin, Prashanth (GER) – Mridangam
6.45 p.m. - Palghat R.Ramprasad (Vocal) , B.V.RaghavendraRao – Violin , Rajesh Salem (USA) – Mridangam

Dec. 25, 2017 - Monday
7,00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
9.40 a.m. - Sudha Ragunathan (Vocal), R.K.Shriramkumar (Violin), K.V.Prasad (Mridangam), Dr.S.Karthick (Ghatam)
4.00 p.m. - Sanjay Subrahmanyan (Vocal), S.Varadarajan (Violin), Neyveli B.Venkatesh (Mridangam), B.Rajasekar (Morsing)
7.00 p.m. - Priyadarsini Govind (Bharatanatyam)
3.00 p.m.- Chithra Lakshmanan (Bharatanatyam)
4.45 p.m. - Venkatasubramanian (Bharatanatyam)

Dec. 26, 2017 - Tuesday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar (Maha Bhaktha Vijayam)
12.10 p.m. - Sadanam Balakrishnan (Kathakali)
4.00 p.m. - U.Rajesh (Mandolin), B.Vijaygopal ( Flute ), S.D.Sridhar (Violin), Madirimangalam Swaminathan (Mridangam), Trichy Murali (Ghatam)
7.00 p.m. - Sujatha Mohapatra (Odissi)
3.00 p.m.- Shradha(Dance)
4.45 p.m.- Deepesh Hoskere (Dance)

Dec. 27, 2017 - Wednesday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
4.00 p.m. - S. Sowmya (Vocal), Nagai Sriram (Violin), Neyveli R.Narayanan (Mridangam), Payyanur T.Govindaprasad (Morsing), G.Chandrasekara Sarma (Ghatam)
7.00 p.m. - Padmavani and Jaikishore Mosalikanti (Kuchipudi)
3.00 p.m.- Hamsa Balagurunathan (Bharatanatyam)
4.45 p.m.- Akshaya Arul (Dance)

Dec. 28, 2017 - Thursday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
4.00 p.m. - Neyveli Santhanagopalan (Vocal), Lalgudi Vijayalakshmi (Violin), Arjun Kumar (Mridangam), Udupi Sridhar (Ghatam)
7.00 p.m. - Anita R.Ratnam(Dance) 
3.00 p.m.- Mridula Sivakumar (Bharatanatyam)
4.45 p.m. - Divya Ravi (Bharatanatyam)

Dec. 29, 2017 - Friday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar (Maha Bhaktha Vijayam)
4.00 p.m. - S. Saketharaman (Vocal), Vittal Ramamurthy (Violin), Mannargudi A.Easwaran (Mridangam), U.N.Giridhar Udupa (Ghatam)
7.00 p.m. - Bhabhi (Mohiniattam)
3.00 p.m.- Meera Anand (Bharatanatyam)
4.45 p.m. - Shreya Iyer (Bharatanatyam)

Dec. 30, 2017- Saturday
7.00 a.m. - Vittaldas Jayakrishna Dikshithar(Maha Bhaktha Vijayam)
10.20 a.m. - Sid Sriram(Vocal)
4.00 p.m. - Shashank Subramanyam ( Flute ),Pandit Debashish Bhattacharya ( Guitar), Guru Kaaraikkudi R.Mani (Mridangam), Pandit Anindo Chatterjee (Tabla)
7.00 p.m. - PadmaSubrahmanyam (Bharathanatyam)
3.00 p.m.- Tvishi Duggal (Bharatanatyam)
4.45 p.m. - Snigdha Menon (Bharatanatyam)

Dec. 31, 2017 - Sunday
7.00 a.m. -  Sengalipuram Brahmasri Vittaldas Jayakrishna Dikshithar Maharaj (Harinamasankeerthanam)
4.00 p.m. - Carnatica Brothers (Vocal), Mysore Dr. Manjunath (Violin), Thiruvarur Bakthavathsalam (Mridangam), K.V.Gopalakrishnan (Kanjira)
7.00 p.m. - Meenakshi Srinivasan (Bharatanatyam)
3.00 p.m.- B. AmrithaVarshini (Bharatanatyam)
4.45 p.m. - Kalaisan Kalaichelvan (Bharatanatyam)

Jan. 1, 2018 - Monday
7.00 a.m. - Sengalipuram Brahmasri Vittaldas Jayakrishna Dikshithar Maharaj(Harinamasankeerthanam)
4.00 p.m. - Athul Kumar (Flute), M.A.Sundareswaran (Violin), K.Sai Giridhar (Mridangam), Dr.S.Karthick (Ghatam)
3.00 p.m.- Sushmitha Rajtilak (Bharatanatyam)
4.45 p.m. - Priyanka .R  (Kuchipudi)

Jan.2. 2018 - Tuesday
6.00 p.m. - M.K.S.Siva (Nadaswaram), Veliambakkam Palanivel (Spl.Thavil), Mambalam S.P. Arulanandham (Spl.Thavil)
3.00 p.m.- G. Bagya Lakshmi (Bharatanatyam)
4.45 p.m. - Sneha Chakrapani (Bharatanatyam)

Jan. 3, 2018 - Wednesday
6.00 p.m. - K. J. Yesudas (Vocal), Nagai Muralidharan (Violin), B.Harikumar (Mridangam), Vaikom Gopalakrishnan (Ghatam)
3.00 p.m.- S. Keerthana (Bharatanatyam)
4.45 p.m. - Aishwarya Krishnakumar (Bharatanatyam)
